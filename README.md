# UPI install on AWS

1. Follow https://access.redhat.com/documentation/en-us/openshift_container_platform/4.1/html/installing/installing-on-user-provisioned-aws

## Additional information to specific sections of this tutorial

2.1.7.3 "Launch the template":
`aws cloudformation create-stack --stack-name openshift-cf-vpc --template-body file://cloudformation/vpc/template.yaml --parameters file://cloudformation/vpc/parameters.json`

2.1.7.4.b Get the required information for the stack
`aws cloudformation describe-stacks --stack-name openshift-cf-vpc | jq .Stacks[].Outputs > cloudformation/vpc/outputs.json`

2.1.8.5 "Launch the template":
`aws cloudformation create-stack --stack-name openshift-cf-network --template-body file://cloudformation/network_lb/template.yaml --parameters file://cloudformation/network_lb/parameters.json --capabilities CAPABILITY_NAMED_IAM`

2.1.8.6.b Get the required information for the stack
`aws cloudformation describe-stacks --stack-name openshift-cf-network | jq .Stacks[].Outputs > cloudformation/network_lb/outputs.json`

2.1.9.3 "Launch the template":
`aws cloudformation create-stack --stack-name openshift-cf-sg --template-body file://cloudformation/sg_roles/template.yaml --parameters file://cloudformation/sg_roles/parameters.json --capabilities CAPABILITY_NAMED_IAM`

2.1.10.4 "Launch the template":
`aws cloudformation create-stack --stack-name openshift-cf-bootstrap --template-body file://cloudformation/bootstrap/template.yaml --parameters file://cloudformation/bootstrap/parameters.json --capabilities CAPABILITY_NAMED_IAM`